# Read Me

This repo contains a collection of centos 7.x config files for use with mock.

# Information

#### File: centos-7-x86_64.cfg

File to use when you want to build a package with all latest updates. This is
the default config and one you will make most use of.

#### File: enterprise-7-x86_64.cfg

As 'centos-7-x86_64.cfg' without the extended 'dist' tag e.g. 'centos'.
Resulting packages name will contain 'el7'.

#### File: enterprise-epel-7-x86_64.cfg

As 'enterprise-7-x86_64.cfg' without the extended 'dist' tag e.g. 'centos'
and adds EPEL. Resulting packages name will contain 'el7.epel'.

#### File: enterprise-epel-scl-7-x86_64.cfg

As 'enterprise-7-x86_64.cfg' without the extended 'dist' tag e.g. 'centos'
and adds EPEL. Adds build deps for SCLO. Resulting packages name will contain
'el7.epel'.

#### File: enterprise-scl-7-x86_64.cfg

As 'enterprise-7-x86_64.cfg' without the extended 'dist' tag e.g. 'centos'.
Adds build deps for SCLO Resulting packages name will contain 'el7'.
